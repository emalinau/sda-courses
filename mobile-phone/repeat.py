active = True

while(active):
    testStr = ""
    try:
        testStr = input("Please enter string to test: ")
    except ValueError as e:
        print(f"Something bad happened: {e}")
        testStr = input("Please try again")
    

    if (testStr.lower() == "exit" or testStr.lower() == "quit"):
        active = False
        print("Shutting down...")
    else:            
        length = len(testStr)
        avg = int(length / 2) 
        even = length % 2 == 0 
        if (length >= 10):
            if (even):
                print(f"4 character long substring -> {testStr[avg-2:avg+2]}")
            else:
                print(f"5 character long substring -> {testStr[avg-2:avg+3]}")
        else:
            print("The string provided needs to be atleast 10 characters long!")