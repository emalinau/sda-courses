class Contact:
    def __init__(self, name: str, number: str):
        self.__name = name
        self.__number = number

    @staticmethod
    def createContact(name: str, number: str):
        return Contact(name, number)

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    @name.deleter
    def name(self):
        del self.__name

    @property
    def number(self) -> str:
        return self.__number

    @number.setter
    def number(self, number: str):
        self.__number = number

    @number.deleter
    def number(self):
        del self.__number
