from typing import Optional
from contact import Contact
from mobilePhone import MobilePhone


class Main:
    mobile = MobilePhone()

    @staticmethod
    def main():
        Main.startPhone()
        active = True
        Main.printOptions()

        while (active):
            num: Optional[int] = None
            try:
                num = int(input("Enter an option number: ").strip())
            except ValueError:
                print("Illegal value entered!")

            if (num == 0):
                Main.printOptions()
            elif (num == 1):
                Main.mobile.printContactList()
            elif (num == 2):
                Main.addContact()
            elif (num == 3):
                Main.modifyContact()
            elif (num == 4):
                Main.removeContact()
            elif (num == 5):
                Main.queryContact()
            elif (num == 6):
                active = False
                print("Shutting down...")
            else:
                print("Illegal input! Please try again. ")

    @staticmethod
    def printOptions():
        print("\nPress")
        print("\t 0 - To print choice options.")
        print("\t 1 - To print contact list.")
        print("\t 2 - To add a contact to the list.")
        print("\t 3 - To modify a contact in the list.")
        print("\t 4 - To remove a contact from the list.")
        print("\t 5 - To search for a contact in the list.")
        print("\t 6 - To turn off your phone.")

    @staticmethod
    def addContact():
        name = input("Enter contact name: ")
        number = input("Enter contact number: ")
        newContact = Contact.createContact(name, number)
        if (Main.mobile.addContact(newContact)):
            print(f"New contact added: {name}, phone number {number}")
        else:
            print(f"{name} is already on file.")

    @staticmethod
    def modifyContact():
        currentContactName = input("Enter current contact name: ")
        existingContactRecord = Main.mobile.queryContactObject(currentContactName)
        if (existingContactRecord is None):
            print("Contact not found!")
            return
        newContactName = input("Enter new contact name: ")
        newContactNumber = input("Enter new contact's number: ")
        newContact = Contact.createContact(newContactName, newContactNumber)
        if (Main.mobile.modifyContact(existingContactRecord, newContact)):
            print("Successfully updated record")
        else:
            print("Error updating record")

    @staticmethod
    def removeContact():
        contactName = input("Enter contact name to remove: ")
        existingContactRecord = Main.mobile.queryContactObject(contactName)
        if (existingContactRecord is None):
            print("Contact not found!")
            return

        if (Main.mobile.removeContact(existingContactRecord)):
            print("Successfully deleted")
        else:
            print("Error while deleting contact")

    @staticmethod
    def startPhone():
        print("Starting phone...")

    @staticmethod
    def queryContact():
        name = input("Enter existing contact name: ")
        existingContactRecord = Main.mobile.queryContactObject(name)
        if (existingContactRecord is None):
            print("Contact not found")
            return
        print(f"Name: {existingContactRecord.name}, phone number is {existingContactRecord.number}")


Main.main()
