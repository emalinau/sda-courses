from contact import Contact
from typing import List, Optional


class MobilePhone:
    def __init__(self):
        self.__contacts: List[Contact] = []

    def addContact(self, contact: Contact) -> bool:
        """
            Method to add a {Contact} object to list if name of {Contact} wasn't found already
            :param contact: to test
            :return: If method was successful
        """
        if (self.__searchContactName(contact.name) >= 0):
            print("Contact already exists!")
            return False
        else:
            self.__contacts.append(contact)
            return True

    def printContactList(self):
        """
            Method to print contacts from list
        """
        print(f"Contacts in list {len(self.__contacts)}")
        for i in range(0, len(self.__contacts)):
            print(f"{i + 1}. {self.__contacts[i].name} -> {self.__contacts[i].number}")

    def modifyContact(self, oldContact: Contact, newContact: Contact) -> bool:
        """
            Method to modify {Contact} object from list if it was found.
            :param oldContact: to query
            :param newContact: New object to replace oldContact with
            :return: If modification was successful.
        """
        position = self.__searchContactObject(oldContact)
        if (position <= 0):
            print(f"{oldContact.name} was not found!")
            return False
        else:
            self.__contacts[position] = newContact
            print(f"{oldContact.name} was replaced by {newContact.name}")
            return True

    def removeContact(self, contact: Contact) -> bool:
        """
            Method to remove {Contact} object from list if it was found.
            :param contact: to query
            :return: If removal was successful.
        """
        position = self.__searchContactObject(contact)
        foundContact = self.__contacts[position]
        if (position >= 0):
            self.__contacts.remove(foundContact)
            print(f"{contact.name} was deleted")
            return True
        else:
            print(f"{contact.name} was not found.")
            return False

    def queryContactName(self, contact: Contact) -> Optional[str]:
        """
            Method to retrieve {Contact} object's name
            :param contact: to test
            :return: Name of contact object in list
        """
        if (self.__searchContactObject(contact) >= 0):
            return contact.name

        return None

    def queryContactObject(self, name: str) -> Optional[Contact]:
        """
            Method to retrieve {Contact} object
            :param name: Finding index based on name
            :return: Contact object if index is in list
        """
        position = self.__searchContactName(name)
        if (position >= 0):
            return self.__contacts[position]
        else:
            return None

    def __searchContactObject(self, contact: Contact) -> int:
        """
            Method to retrieve index of {Contact} object
            :param contact to test
            :returns: index of Contact object
        """
        return self.__contacts.index(contact)

    def __searchContactName(self, name: str) -> int:
        """
            Method to retrieve index of {Contact} object's name
            :param name: to test
            :return: index of Contact object
        """
        for i in range(0, len(self.__contacts)):
            if (self.__contacts[i].name == name):
                return i

        return -1


