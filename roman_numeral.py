import re


class Converter:
    def __getDigits(self, x):
        numbers = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
        values = [4, 9, 40, 90, 400, 900, 1, 5, 10, 50, 100, 500, 1000]
        for i in range(0, len(numbers)):
            length = len(numbers[i])
            if (str(x)[0:length] == numbers[i]):
                return [values[i], length, numbers[i]]

        return [0, 0, 0]

    def roman2num(self, numeral):
        numeral = re.sub('/s', '', str(numeral)) # Wanted to remove whitespace; doesn't work
        formattedNumeral = numeral
        txt = ""
        txt1 = ""
        txt2 = ""
        numeral = numeral.upper()
        romanValue = 0
        while(len(numeral) > 0):
            digits = self.__getDigits(numeral)
            romanValue += digits[0]
            if (digits[1] == 0):
                return 0
            if (digits[1] == 1):
                numeral = numeral[1:len(numeral)]
            if (digits[1] == 2):
                numeral = numeral[2:len(numeral)]
            txt1 += digits[2] + "+"
            txt2 += str(digits[0]) + "+"

        txt1 = txt1[0:len(txt1) - 1]
        txt2 = txt2[0:len(txt2) - 1]
        if (formattedNumeral == txt1):
            txt1 = ""
        else:
            txt1 = " = " + txt1
        if (txt2 == romanValue):
            txt2 = ""
        else:
            txt2 += " = "
        txt = formattedNumeral + txt1 + " = " + txt2 + str(romanValue)
        txt = txt.upper()
        return [romanValue, txt]



invalidCharacters = "ABEFGHJKNOPQRSTUWYZ"
numeral = input("Enter your numeral: ")
converter = Converter()
result = -1
for invalid in invalidCharacters:
    if (invalid in numeral.upper()):
        print("Invalid numeral typed")
        break
    else:
        result = converter.roman2num(numeral)

print(result)