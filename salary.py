# Class is a blueprint for an object. In this case, Employee object.
class Employee:
    # This is a constructor for the class which creates an object
    def __init__(self, name, lastName, age, salary):
        # These are class fields or properties
        self.name = name
        self.lastName = lastName
        self.age = age
        self.salary = salary

    # Class method which simply prints a formatted string into console
    def printCell(self):
        fullName = self.name + ' ' + self.lastName
        print(f"| {fullName:15} | {self.age:5} | {self.salary:12} |")


# A collection of employee objects
emp_arr = [
    Employee("John", "Doe", 27, 123456.00), 
    Employee("John", "Wick", 40, 50000.00), 
    Employee("Jeff", "Bezos", 45, 999999999.95)
]

# A 'table' header gets formatted here
print(f"| {'Name':15} | {'Age':5} | {'Salary':12} |")
print("-" * 42)
# A loop for the employee collection created early. Each employee object calls its' .printCell() method in the class
for emp in emp_arr:
    emp.printCell()