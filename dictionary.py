# dictionary = {
#     "a" : [1, 2, 3],
#     "b" : "Hello World",
#     "c" : {
#         "dictionary" : "in a dictionary"
#     }
# }


# dictionary["d"] = 1337
# dictionary["a"] = [4, 5, 6]
# print(dictionary)


# dictionary = {
#     "a" : {
#         "dictionary" : "in a dictionary",
#         "and another": {
#             "dictionary" : {
#                 "within " : {
#                     "a dictionary" : {
#                         "which dictionary index" : "is <b>this</b>"
#                     }
#                 }
#             }
#         }
#     }
# }


# TUPLES

# myTuple = "a", "Hello World!", None, True, 57.5
# for v in myTuple:
#     if (v == "a"):
#         continue
#     print(v)

# print(len(myTuple))


# print(myTuple.index("a"))

# SETS

# mySet = {"Captain", "Jean", "Luc", "Picard"}
# anotherSet = mySet.copy()

# anotherSet.add(1337)

# anotherSet.discard("Luc")
# print(anotherSet)

# Check pages 80-130
