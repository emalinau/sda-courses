class Calculator:
    def __init__(self):
        print("Creating Calculator object")

    # Function for adding
    def add(self, x, y):
        if (x == None or y == None):
            print("One or both values are missing")
            return -1
        return x + y

    # Function for subtracting
    def min(self, x, y):
        if (x == None or y == None):
            print("One or both values are missing")
            return -1
        return x - y

    # Function for multiplication
    def mult(self, x, y):
        if (x == None or y == None):
            print("One or both values are missing")
            return -1
        return x * y

    # Function for division
    def div(self, x, y):
        if (x == None or y == None):
            print("One or both values are missing")
            return -1
        return x / y

    # Function for remainder
    def modal(self, x, y):
        if (x == None or y == None):
            print("One or both values are missing")
            return -1
        return x % y



calc = Calculator()
arr = [10, 5]


# print("Adding %d with %d equals to %d" % (arr[0], arr[1], calc.add(arr[0], arr[1])))
# print("Subtracting %d from %d equals to %d" % (arr[0], arr[1], calc.min(arr[0], arr[1])))
# print("Multiplying %d with %d equals to %d" % (arr[0], arr[1], calc.mult(arr[0], arr[1])))
# print("Dividing %d from %d equals to %d" % (arr[0], arr[1], calc.div(arr[0], arr[1])))
# print("Remainder of %d from %d equals to %d" % (arr[0], arr[1], calc.modal(arr[0], arr[1])))

# for x in arr:
#     if (x == 5):
#         continue

#     print(x)

# Tekstas kurį tikrinam
testStr = "Lorem ipsum..."
# Gaunam teksto ilgį
length = len(testStr)
# Gaunam teksto ilgio vidurkį
avg = int(length / 2) 
# Bool reikšmė ar teksto ilgio skaičius lyginis
even = length % 2 == 0 
# Jei teksto ilgis yra daugiau arba lygus 10 tęsti bloke
if (length >= 10):
     # Jei tekstas lyginis print 4-ių ilgio vidurinį tekstą
    if (even):
        print(testStr[avg-2:avg+2])
    # Jei nelyginis print 5-ių ilgio vidurinį tekstą
    else:
        print(testStr[avg-2:avg+3])
# Jei tekstas nėra 10 ar daugiau ilgio, tęsti veiksmą čia
else:
    print("The string provided needs to be atleast 10 characters long!")


# arr1 = ["f", "a", "c", "l", "e", "b"]
# arr1.sort()
# print(arr1)

tekstas= input("Irasykite sakini:")
 
while(tekstas != "quit"):
    stringas = tekstas
    ilgis = len(stringas)
    vidurkis = int(ilgis / 2)
    even = ilgis % 2 == 0
    if (ilgis >= 10):
        if (even):
            print(stringas[vidurkis-2:vidurkis+2])
        else:
            print(stringas[vidurkis-2:vidurkis+3])
    else:
        print("Klaida.")
